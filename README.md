# EducationEverywhere

an open and flexible learning experience

* with ActivityPub: [@tqt@hub.disroot.org](https://hub.disroot.org/@tqt)

* on Matrix: **@tqt:privacytools.io** & **#tqtee:privacytools.io**

Thanks to the ongoing support of [disroot.org](https://disroot.org) we are better able to develop and extend the functions of our organisation, by utilising only _free, libre and open works_.


Our informal **chat room** is located at: [mumble.disroot.org](https://mumble.disroot.org) 
- for **P2P, 1-to-1 & group conferencing** (by prior arrangement & mutual agreement), please visit: [calls.disroot.org](https://calls.disroot.org/tqtee)


#### **_coming soon!!_**  https://ee.tqt.solutions